# EJERCICIO 19.1. Clase contentApp

import webapp

PAGINA_ENCONTRADA = """
<!DOCTYPE html>
<html lang="en">
  <body>
    {contenido}
  </body>
</html>
"""

PAGINA_NO_ENCONTRADA = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>El recurso {recurso} no fue encontrado.</p>
  </body>
</html>
"""


class contentApp(webapp.webApp):

    diccionario = {'/': "<p>P&aacute;gina principal</p>",
                '/holamundo': "<p>Hola mundo!</p>",
                '/adiosmundo': "<p>Adi&oacute;s mundo!</p>"}

    def parse(self, received):
        recurso = received.decode().split(' ')[1]
        return recurso

    def process(self, analyzed):
        if analyzed not in self.diccionario:
            codigo = "404 Not Found"
            pagina = PAGINA_NO_ENCONTRADA.format(recurso=analyzed)
        else:  # if analyzed in self.diccionario:
            codigo = "200 OK"
            contenido = self.diccionario[analyzed]
            pagina = PAGINA_ENCONTRADA.format(contenido=contenido)
        return (codigo, pagina)


if __name__ == "__main__":
    testContentApp = contentApp('localhost', 1234)
